import SwiftyJSON

public protocol JSONSerializable {
  var data: JSON { get }
  func toJSON() -> JSON
}

extension JSONSerializable {
  public func toJSON() -> JSON { return data }
}

extension Sequence where Iterator.Element: JSONSerializable {
  public func toJSON() -> JSON { return JSON(self.map { $0.data }) }
}
