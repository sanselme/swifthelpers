// swift-tools-version:3.0
import PackageDescription

let urls: [String] = [
  "https://github.com/swiftyjson/swiftyjson"
]

let targets: [[String: Any]] = [
  ["name": "SwiftHelpers"]
]

let package = Package(
    name: "SwiftHelpers",
    targets: targets.flatMap { target in
      guard let name = target["name"] else { return Target(name: "") }
      guard let dependencies = target["dependencies"] else { return Target(name: name as! String) }

      return Target(name: name as! String, dependencies: (dependencies as! [String]).map { Target.Dependency(stringLiteral: $0) })
    },
    dependencies: urls.map { .Package(url: $0, versions: Version(.min, .min, .min) ..< Version(.max, .max, .max)) }
)
